module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    browsers: "> 0.5%, ie >= 11"
                },
                modules: false,
                spec: true,
                useBuiltIns: false,
                forceAllTransforms: true

            }
        ]
    ],
    "plugins": [
        ["@babel/transform-runtime",
            {
                "regenerator": true,
                "corejs": false,
                "helpers": true,
            }
        ],
        ["replace-dynamic-import-runtime"]
    ],
    sourceMaps: false, inputSourceMap: false
};