

import { bundleTemplates } from "sk-cl-tools";


bundleTemplates('antd-templates.json', "gridy-grid-antd");
bundleTemplates('antd-templates.json', "gridy-grid-antd", "hg");
bundleTemplates('jquery-templates.json', "gridy-grid-jquery");
bundleTemplates('jquery-templates.json', "gridy-grid-jquery", "hg");
bundleTemplates('default-templates.json', "gridy-grid-default");
bundleTemplates('default-templates.json', "gridy-grid-default", "hg");


