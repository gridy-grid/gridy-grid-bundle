
import babel from '@rollup/plugin-babel';
import nodeResolve from '@rollup/plugin-node-resolve';
import commonJS from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';

export default [
  {
    input: 'index.js',
    output: {
      globals: {
      },
      file: 'dist/gridy-grid-bundle.js',
      format: 'umd',
      name: 'window',
      extend: true,
      sourcemap: true,
    },
    external: [  ],
    plugins: [
      json(),
      nodeResolve({ mainFields: ['jsnext:main']}),
      commonJS({ include: "node_modules/**" }),
      babel({ babelHelpers: 'runtime' }),
    ]
  }

];
