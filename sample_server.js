var fs = require('fs');

var path = require('path');
var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');



app.options('*', cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

function sleep(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
}

function parseBody(body) {
    let data = {};
    for (let token of Object.keys(body)) {
        let allFields = body[token].split('Content-Disposition: form-data;');
        for (let field of allFields) {
            let fieldData = field.split('\r\n');
            let name = fieldData[0].match(/"(.*)"/)[1];
            let value = fieldData[2];
            data[name] = value;
        }
    }
    return data;
}

app.post('/submit', cors(), function (req, res, next) {
    //sleep(1000 * 15);
    //res.contentType('application/json');
    //console.log('Got body:', req.body);
    console.log(parseBody(req.body));
    var filePath = path.join(__dirname, 'sample_response.json');
    var readable = fs.createReadStream(filePath);
    readable.pipe(res);
});


function sleep(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
}

app.get('/unpaged', cors(), function (req, res, next) {
    //sleep(1000 * 15);
    res.contentType('application/json');
    var filePath = path.join(__dirname, 'sample_data.json');
    var readable = fs.createReadStream(filePath);
    readable.pipe(res);
});

app.get('/paged', cors(), function (req, res, next) {
    //sleep(1000 * 15);
    res.contentType('application/json');
    var filePath = path.join(__dirname, 'sample_data.json');
    let rawdata = fs.readFileSync(filePath);
    let sample = JSON.parse(rawdata);

    let page = req.query.page || 1;
    let perPage = req.query.perPage || 10;
    let from = (page - 1) * perPage;
    let to = from + perPage;

    var field = req.query.sortField;

    if (field) {
        var direction = req.query.sortDirection || 'asc';

        var sortFunc = (a, b) => {
            let aVal = null;
            let bVal = null;
            if (field && field.startsWith('$')) {
                field = field.slice(2);
            }
            aVal = a[field];
            bVal = b[field];

            if (aVal === bVal) {
                return 0;
            }
            if (direction === undefined || direction === 'asc') {
                if (typeof aVal === 'string') {
                    return aVal.localeCompare(bVal);
                } else {
                    return aVal - bVal;
                }
            } else {
                if (typeof aVal === 'string') {
                    return bVal.localeCompare(aVal);
                } else {
                    return bVal - aVal;
                }
            }

        };

        sample.data.sort(sortFunc);
    }



    sample.data = sample.data.slice(from, to);
    sample.page = page;
    sample.perPage = perPage;
    res.json(sample);
});

app.use(express.static('.'));

app.listen(8080, function () {
    console.log('CORS-enabled web server listening on port 8080')
});
