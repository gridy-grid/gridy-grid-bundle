# Gridy Grid All-in-One Bundle

This is Gridy Grid datatables library bundle can be used to load all needed 
Gridy Grid code with one file for convenience or to have wide browser support 
including legacy versions like Internet Explorer 11.


This package is a prebuilt version of [gridy-grid](https://www.npmjs.com/package/gridy-grid)


## Usage

```
npm i gridy-grid-bundle gridy-grid-antd sk-theme-antd jquery
```

note: jquery is needed to render table with handlebars template engine 
that may be needed for old browsers support and actually optional as 
well as this feature

```html
<script src="/node_modules/jquery/dist/jquery.js"></script>
<script src="/node_modules/gridy-grid-bundle/dist/gridy-grid-bundle.js"></script>
<gridy-grid theme="antd" id="gridyGrid">
    <gridy-data-source dataref="gridData">
        <gridy-data-field title="Title" path="$.title"></gridy-data-field>
        <gridy-data-field title="Price" path="$.price"></gridy-data-field>
        <gridy-data-field title="Created" path="$.created"></gridy-data-field>
    </gridy-data-source>
    <gridy-spinner></gridy-spinner>
    <gridy-filter id="gridyFilter"></gridy-filter>
    <gridy-table id="gridyTable" rd-tpl-fmt="handlebars">
    </gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
    <gridy-table-info id="gridyTableInfo"></gridy-table-info>
</gridy-grid>
<script type="module">
    let data = [];
    for (let i = 0; i < 25; i++) {
        let today = new Date();
        today.setDate(today.getDate() + i);
        data.push({title: 'row' + i, price: 100 * i, created: today.toLocaleDateString("en-US")})
    }
    let grid = document.querySelector('#gridyGrid');
    grid.addEventListener('bootstrap', () => {
        grid.dataSource.loadData(data);
        grid.table.whenRendered(() => {
            console.log('table rendered');
        });
    });
    customElements.define('gridy-grid', GridyGrid);
</script>
```

note: grid-grid-antd is optional and depends on theme of your choice, you will have 
to set tpl-path to folder with templates (gridy-grid.tpl.html, table.tpl.html and others), 
and dt-css-path for styles file in case you not installed theme (-antd, -jquery or -default) package

```html
<script src="/node_modules/gridy-grid-bundle/dist/gridy-grid-bundle.js"></script>
<gridy-grid theme="antd" id="gridyGrid" tpl-path="/tpl/" dt-css-path="tpl/">
    <gridy-data-source dataref="gridData">
        <gridy-data-field title="Title" path="$.title"></gridy-data-field>
        <gridy-data-field title="Price" path="$.price"></gridy-data-field>
    </gridy-data-source>
    <gridy-spinner></gridy-spinner>
    <gridy-filter id="gridyFilter"></gridy-filter>
    <gridy-table id="gridyTable" rd-tpl-fmt="handlebars">
    </gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
    <gridy-table-info id="gridyTableInfo"></gridy-table-info>
</gridy-grid>
```


## Adding filters


To use date filter gridy-filter-date pakage have to be installed or you  to plug preloaded templates.

```
npm i gridy-filter-date
```


```html
<script src="/node_modules/jquery/dist/jquery.js"></script>
<script src="/node_modules/gridy-grid-bundle/dist/gridy-grid-bundle.js"></script>
<sk-config
 lang="ru" id="skConfig" theme="antd"></sk-config>
<gridy-grid theme="antd" id="gridyGrid">
    <gridy-data-source dataref="gridData">
        <gridy-data-field title="Title" path="$.title"></gridy-data-field>
        <gridy-data-field title="Price" path="$.price"></gridy-data-field>
        <gridy-data-field title="Created" path="$.created"></gridy-data-field>
    </gridy-data-source>
    <gridy-spinner></gridy-spinner>
    <gridy-filter dri="date" filter-mode="ge" field-title="Created" id="gridyFilter"></gridy-filter>
    <gridy-table id="gridyTable" rd-tpl-fmt="handlebars">
    </gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
    <gridy-table-info id="gridyTableInfo"></gridy-table-info>
</gridy-grid>
<script type="module">
    let data = [];
    for (let i = 0; i < 25; i++) {
        let today = new Date();
        today.setDate(today.getDate() + i);
        data.push({title: 'row' + i, price: 100 * i, created: today.toLocaleDateString("en-US")})
    }
    let grid = document.querySelector('#gridyGrid');
    grid.addEventListener('bootstrap', () => {
        grid.dataSource.loadData(data);
        grid.table.whenRendered(() => {
            console.log('table rendered');
        });
    });
    customElements.define('gridy-grid', GridyGrid);
    customElements.define('sk-datepicker', SkDatePicker);
</script>
```

## Bundle build

Just pack everything in one file. No transpilations. Esm modules with components will be bundled in one file.

```
npm run build
```

Build with styles inlining

```
npm --tpl-inline-styles=./node_modules/gridy-grid-jquery,./node_modules/sk-theme-jquery,./node_modules/sk-theme-antd run build
```


## Usage

check index.html for example serve it with http server like this:

```
npx http-server
```

